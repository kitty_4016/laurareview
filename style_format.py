from copy import copy
import pandas as pd
import bisect
import random
from openpyxl import load_workbook
from openpyxl.utils import get_column_letter
import os
import shutil
import os
import time

df_new = pd.read_excel('sample_book.xlsx')

file_name = "pandas_header_format.xlsx"


def pandas_to_excel(df, fnm):
    writer = pd.ExcelWriter(
        fnm,
        engine='xlsxwriter',
    )

    # Convert the dataframe to an XlsxWriter Excel object. Note that we turn off
    # the default header and skip one row to allow us to insert a user defined
    # header.

    df.to_excel(writer,
                sheet_name='Sheet1',
                startrow=1,
                header=False,
                index=False)

    # Get the xlsxwriter workbook and worksheet objects.
    workbook = writer.book
    worksheet = writer.sheets['Sheet1']

    # Add a header format.
    header_format = workbook.add_format({
        'bold': True,
        'text_wrap': True,
        'valign': 'top',
        'border': 1,
        'font_name': 'SimHei',
        'font_color': '#FFFFFF',
        'fg_color': '#436089',
    })

    # Write the column headers with the defined format.
    for col_num, value in enumerate(df.columns.values):
        worksheet.write(0, col_num, value, header_format)
    format1 = workbook.add_format({'font_name': 'SimHei'})
    format2 = workbook.add_format({
        'font_name': 'SimHei',
        'font_color': '#B2281D'
    })
    worksheet.set_column('A:A', 8, format2)
    worksheet.set_column('B:B', 100, format1)
    worksheet.set_column('C:C', 20, format1)
    worksheet.set_column('D:D', 80, format1)
    writer.save()


pandas_to_excel(df_new, file_name)
# writer = pd.ExcelWriter(
#     "pandas_header_format.xlsx",
#     engine='xlsxwriter',
# )

# # Convert the dataframe to an XlsxWriter Excel object. Note that we turn off
# # the default header and skip one row to allow us to insert a user defined
# # header.

# df = pd.read_excel('sample_book.xlsx')
# df.to_excel(writer, sheet_name='Sheet1', startrow=1, header=False, index=False)

# # Get the xlsxwriter workbook and worksheet objects.
# workbook = writer.book
# worksheet = writer.sheets['Sheet1']

# # Add a header format.
# header_format = workbook.add_format({
#     'bold': True,
#     'text_wrap': True,
#     'valign': 'top',
#     'border': 1,
#     'font_name': 'SimHei',
#     'font_color': '#FFFFFF',
#     'fg_color': '#436089',
# })

# # Write the column headers with the defined format.
# for col_num, value in enumerate(df.columns.values):
#     worksheet.write(0, col_num, value, header_format)
# format1 = workbook.add_format({'font_name': 'SimHei'})
# format2 = workbook.add_format({'font_name': 'SimHei', 'font_color': '#B2281D'})
# worksheet.set_column('A:A', 8, format2)
# worksheet.set_column('B:B', 100, format1)
# worksheet.set_column('C:C', 20, format1)
# worksheet.set_column('D:D', 80, format1)
# writer.save()

# wb = load_workbook('论文评审意见表.xlsx')

# default_sheet = wb['Sheet1']
# ws = wb.active
# default_col_width = ws.sheet_format.defaultColWidth
# new_sheet = wb.create_sheet('Sheet2')

# row1 = next(default_sheet.rows)

# row1 = next(default_sheet.rows)
# for cell in row1:
#     new_cell = new_sheet.cell(row=cell.row,
#                               column=cell.col_idx,
#                               value=cell.value)
#     print(ws.column_dimensions[get_column_letter(cell.col_idx + 1)].width)
#     if cell.has_style:
#         new_cell.font = copy(cell.font)
#         new_cell.border = copy(cell.border)
#         new_cell.fill = copy(cell.fill)
#         new_cell.number_format = copy(cell.number_format)
#         new_cell.protection = copy(cell.protection)
#         new_cell.alignment = copy(cell.alignment)

# wb.save(filename='sample_book.xlsx')
