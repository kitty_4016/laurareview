import pandas as pd
import bisect
import random
from StyleFrame import StyleFrame, Styler
'''
 该函数主要的参数为io、sheetname、header、names、encoding。
 io:excel文件，可以是文件路径、文件网址、file-like对象、xlrd workbook;
 sheetname:返回指定的sheet，参数可以是字符串（sheet名）、整型（sheet索引）、list（元素为字符串和整型，返回字典{'key':'sheet'}）、none（返回字典，全部sheet）;
 header:指定数据表的表头，参数可以是int、list of ints，即为索引行数为表头;
 names:返回指定name的列，参数为array-like对象。
 encoding:关键字参数，指定以何种编码读取。
 该函数返回pandas中的DataFrame或dict of DataFrame对象，利用DataFrame的相关操作即可读取相应的数据。
 '''
# 代码示例:
excel_path = '作者.xlsx'
d_author = pd.read_excel(excel_path)
d_author_origin = d_author
d_author.dropna(subset=[
    '文章类型 （1实证；2理论；3都有或者不清楚）', '文章领域（1资产定价；2公司金融；3金融中介/银行/金融机构；4宏观金融；5其他）'
],
                inplace=True)
# print(len(d_author))
excel_path = 'Copy of 评委_jb.xlsx'
d_reviewer = pd.read_excel(excel_path, sheet_name='评论人')
d_reviewer_origin = d_reviewer
# print(d_reviewer.head())
# print(len(d_reviewer))

d_reviewer.applymap(lambda x: x.strip() if isinstance(x, str) else x)
print(d_reviewer['序号'].tail(20))
d_reviewer = d_reviewer.loc[d_reviewer['是否可以做评委'] == 'Y']
print(len(d_reviewer))
print(d_reviewer['序号'].tail(20))
# print(len(d_reviewer))
# print(d_reviewer.head())
d_reviewer.fillna(0, inplace=True)
# print(d_reviewer.head())
# print(d_author.head())
# d_author['reviewer_id'] =
#
# d_author['reviewer_id'] = ([[]] * len(d_author)).copy()
d_author["org"] = d_author['单位1'] + d_author['单位2'] + d_author[
    '单位3'] + d_author['单位4'] + d_author['单位5']
d_author.fillna("", inplace=True)
d_author['org'] = d_author[['单位1', '单位2', '单位3', '单位4', '单位5']].agg('-'.join,
                                                                    axis=1)
d_author['reviewer_id'] = d_author['org']
# print(d_author['org'].head())
# print(d_author.head())
# print()
all_list = []
for d_author_index in range(len(d_author)):

    newlist = []
    for d_reviewer_index in range(len(d_reviewer)):
        if d_reviewer.iloc[d_reviewer_index, :]['学校'] in (
                d_author.iloc[d_author_index, :]['org']):
            # print("reviewer id: {} author id: {}".format(
            #     d_reviewer.iloc[d_reviewer_index, :]['序号'],
            #     d_author.iloc[d_author_index, :]['序号']))
            # print("list")
            # print(d_author.iloc[d_author_index, :]['reviewer_id'])
            # print("number")
            # print(d_reviewer.iloc[d_reviewer_index, :]['序号'])
            newlist.append(d_reviewer.iloc[d_reviewer_index, :]['序号'])
            #  = d_author.iloc[
            #     d_author_index, :]['reviewer_id'].append()
    all_list.append(newlist)
d_author['reviewer_id'] = all_list

d_author['flag'] = d_author.apply(lambda row: 1
                                  if len(row['reviewer_id']) == 0 else 0,
                                  axis=1)


def row_style(row):
    if row.flag == 1:
        return pd.Series('background-color: red', row.index)
    else:
        return pd.Series('background-color: gray', row.index)


print("sum {}".format(sum(d_author['flag'].values.tolist())))
d_author.style.apply(row_style, axis=1).to_excel('论文_筛选.xlsx', index=True)
d_author.loc[d_author['flag'] == 1, :].to_excel('论文_筛选_new.xlsx', index=False)
# d_author['reviewer_number'] = len(d_author['reviewer_id'].values.tolist())
# print(d_author[['reviewer_id']].head(20).values.tolist())

d_author_full = d_author
reviewer_list_type_0 = d_reviewer[d_reviewer['单位类别'] ==
                                  0]['序号'].values.tolist()
reviewer_list_type_1 = d_reviewer[d_reviewer['单位类别'] ==
                                  1]['序号'].values.tolist()
reviewer_list_type_2 = d_reviewer[d_reviewer['单位类别'] ==
                                  2]['序号'].values.tolist()
reviewer_list_type_3 = d_reviewer[d_reviewer['单位类别'] ==
                                  3]['序号'].values.tolist()

reviewer_list_type_theory = d_reviewer[d_reviewer['是否理论'] ==
                                       1]['序号'].values.tolist()

print(reviewer_list_type_theory)
# print(reviewer_list_type_1)
# print(reviewer_list_type_2)
# print(reviewer_list_type_3)

d_author_type_1 = d_author_full[
    d_author_full['文章类型 （1实证；2理论；3都有或者不清楚）'] == 1].reset_index().loc[:, [
        '序号', '文章类型 （1实证；2理论；3都有或者不清楚）',
        '文章领域（1资产定价；2公司金融；3金融中介/银行/金融机构；4宏观金融；5其他）', 'reviewer_id'
    ]]
d_author_type_2 = d_author_full[
    d_author_full['文章类型 （1实证；2理论；3都有或者不清楚）'] == 2].reset_index().loc[:, [
        '序号', '文章类型 （1实证；2理论；3都有或者不清楚）',
        '文章领域（1资产定价；2公司金融；3金融中介/银行/金融机构；4宏观金融；5其他）', 'reviewer_id'
    ]]
d_author_type_3 = d_author_full[
    d_author_full['文章类型 （1实证；2理论；3都有或者不清楚）'] == 3].reset_index().loc[:, [
        '序号', '文章类型 （1实证；2理论；3都有或者不清楚）',
        '文章领域（1资产定价；2公司金融；3金融中介/银行/金融机构；4宏观金融；5其他）', 'reviewer_id'
    ]]
global_banned_list = []


def index_to_column(index_numnber):
    if index_numnber == 1:
        return "1资产定价"
    if index_numnber == 2:
        return "2公司金融"
    if index_numnber == 3:
        return "3金融中介/银行/金融机构"
    if index_numnber == 4:
        return "4宏观金融"
    if index_numnber == 5:
        return "5.其他（主要是计量方向）"


# print(d_author_type_1)
reviewer_list_all = []


def diff(first, second):
    second = set(second)
    return [item for item in first if item not in second]


def find_best_from_candiate(category, reviewer_list, reviewer_list_pd):

    return [
        reviewer_id for reviewer_id in reviewer_list
        if (reviewer_list_pd.loc[reviewer_list_pd['序号'] == reviewer_id, :][
            index_to_column(category)].values == 1)
    ]


reviewer_list_all_list = []
banner_list_global = []

global_reviewer_count = {}
for reviewer_id_temp in d_reviewer['序号'].values.tolist():
    global_reviewer_count[reviewer_id_temp] = 0


def check_banner(reviewer_id_list):

    reviewers_over = []
    for id_temp in reviewer_id_list:

        if d_reviewer.loc[d_reviewer['序号'] == id_temp, '单位类别'].values == 0:
            max_number = 23
        else:
            max_number = 14
        if global_reviewer_count[id_temp] >= max_number:
            reviewers_over.append(id_temp)
        else:
            global_reviewer_count[id_temp] = global_reviewer_count[id_temp] + 1

    return reviewers_over


# type 2 理论
for i in range(len(d_author_type_2)):

    reviewer_list_part = []
    banned_list = d_author_type_2.iloc[i, :]['reviewer_id']
    banned_list = banned_list + global_banned_list
    print("theosry list{}".format(reviewer_list_type_theory))
    # 2个0号单位评委；

    candidate_list_all = diff(reviewer_list_type_theory, banned_list)
    print("candiate_list_all {}".format(candidate_list_all))
    candidate_list_new = find_best_from_candiate(
        d_author_type_2.iloc[i, :]
        ['文章领域（1资产定价；2公司金融；3金融中介/银行/金融机构；4宏观金融；5其他）'], candidate_list_all,
        d_reviewer)
    if len(candidate_list_new) >= 2:
        random.seed(1)

        random.shuffle(candidate_list_new)
        reviewer_list_part = reviewer_list_part + (random.sample(
            candidate_list_new, 2))
    else:
        reviewer_list_part = reviewer_list_part + candidate_list_new
        random.seed(2)

        reviewer_list_part = reviewer_list_part + \
            (random.sample(diff(candidate_list_all, candidate_list_new), 2-len(candidate_list_new)))

    # 1个1号单位评委；
    candidate_list_all = diff(reviewer_list_type_1, banned_list)

    candidate_list_new = find_best_from_candiate(
        d_author_type_2.iloc[i, :]
        ['文章领域（1资产定价；2公司金融；3金融中介/银行/金融机构；4宏观金融；5其他）'], candidate_list_all,
        d_reviewer)
    if len(candidate_list_new) >= 1:
        random.seed(3)

        reviewer_list_part = reviewer_list_part + (random.sample(
            candidate_list_new, 1))
    else:
        reviewer_list_part = reviewer_list_part + candidate_list_new
        random.seed(4)

        reviewer_list_part = reviewer_list_part + \
            (random.sample(diff(candidate_list_all, candidate_list_new), 1-len(candidate_list_new)))

    # 1个2号单位评委；
    candidate_list_all = diff(reviewer_list_type_2, banned_list)

    candidate_list_new = find_best_from_candiate(
        d_author_type_2.iloc[i, :]
        ['文章领域（1资产定价；2公司金融；3金融中介/银行/金融机构；4宏观金融；5其他）'], candidate_list_all,
        d_reviewer)
    if len(candidate_list_new) >= 1:
        reviewer_list_part = reviewer_list_part + (random.sample(
            candidate_list_new, 1))
    else:
        reviewer_list_part = reviewer_list_part + candidate_list_new
        reviewer_list_part = reviewer_list_part + \
            (random.sample(diff(candidate_list_all, candidate_list_new), 1-len(candidate_list_new)))

    # 1个3号单位评委。
    candidate_list_all = diff(reviewer_list_type_3, banned_list)

    candidate_list_new = find_best_from_candiate(
        d_author_type_2.iloc[i, :]
        ['文章领域（1资产定价；2公司金融；3金融中介/银行/金融机构；4宏观金融；5其他）'], candidate_list_all,
        d_reviewer)
    if len(candidate_list_new) >= 1:
        reviewer_list_part = reviewer_list_part + (random.sample(
            candidate_list_new, 1))
    else:
        reviewer_list_part = reviewer_list_part + candidate_list_new
        reviewer_list_part = reviewer_list_part + \
            (random.sample(diff(candidate_list_all, candidate_list_new), 1-len(candidate_list_new)))

    global_banned_list = global_banned_list + check_banner(reviewer_list_part)
    reviewer_list_all_list.append(reviewer_list_part)
d_author_type_2['reviewer_assing'] = reviewer_list_all_list
# type 3 混合
reviewer_list_all_list = []
for i in range(len(d_author_type_3)):

    reviewer_list_part = []
    banned_list = d_author_type_3.iloc[i, :]['reviewer_id']
    banned_list = banned_list + global_banned_list

    # 2个0号单位评委；

    candidate_list_all = diff(reviewer_list_type_0, banned_list)
    random.shuffle(candidate_list_all)
    candidate_list_new = find_best_from_candiate(
        d_author_type_3.iloc[i, :]
        ['文章领域（1资产定价；2公司金融；3金融中介/银行/金融机构；4宏观金融；5其他）'], candidate_list_all,
        d_reviewer)
    if len(candidate_list_new) >= 2:
        reviewer_list_part = reviewer_list_part + (random.sample(
            candidate_list_new, 2))
    else:
        reviewer_list_part = reviewer_list_part + candidate_list_new
        reviewer_list_part = reviewer_list_part + \
            (random.sample(diff(candidate_list_all, candidate_list_new), 2-len(candidate_list_new)))

    # 1个1号单位评委；
    candidate_list_all = diff(reviewer_list_type_1, banned_list)

    candidate_list_new = find_best_from_candiate(
        d_author_type_3.iloc[i, :]
        ['文章领域（1资产定价；2公司金融；3金融中介/银行/金融机构；4宏观金融；5其他）'], candidate_list_all,
        d_reviewer)
    if len(candidate_list_new) >= 1:
        reviewer_list_part = reviewer_list_part + (random.sample(
            candidate_list_new, 1))
    else:
        reviewer_list_part = reviewer_list_part + candidate_list_new
        reviewer_list_part = reviewer_list_part + \
            (random.sample(diff(candidate_list_all, candidate_list_new), 1-len(candidate_list_new)))

    # 1个2号单位评委；
    candidate_list_all = diff(reviewer_list_type_2, banned_list)

    candidate_list_new = find_best_from_candiate(
        d_author_type_3.iloc[i, :]
        ['文章领域（1资产定价；2公司金融；3金融中介/银行/金融机构；4宏观金融；5其他）'], candidate_list_all,
        d_reviewer)
    if len(candidate_list_new) >= 1:
        reviewer_list_part = reviewer_list_part + (random.sample(
            candidate_list_new, 1))
    else:
        reviewer_list_part = reviewer_list_part + candidate_list_new
        reviewer_list_part = reviewer_list_part + \
            (random.sample(diff(candidate_list_all, candidate_list_new), 1-len(candidate_list_new)))

    # 1个3号单位评委。
    candidate_list_all = diff(reviewer_list_type_3, banned_list)

    candidate_list_new = find_best_from_candiate(
        d_author_type_3.iloc[i, :]
        ['文章领域（1资产定价；2公司金融；3金融中介/银行/金融机构；4宏观金融；5其他）'], candidate_list_all,
        d_reviewer)
    if len(candidate_list_new) >= 1:
        reviewer_list_part = reviewer_list_part + (random.sample(
            candidate_list_new, 1))
    else:
        reviewer_list_part = reviewer_list_part + candidate_list_new
        reviewer_list_part = reviewer_list_part + \
            (random.sample(diff(candidate_list_all, candidate_list_new), 1-len(candidate_list_new)))

    global_banned_list = global_banned_list + check_banner(reviewer_list_part)
    reviewer_list_all_list.append(reviewer_list_part)
d_author_type_3['reviewer_assing'] = reviewer_list_all_list

reviewer_list_all_list = []
for i in range(len(d_author_type_1)):

    reviewer_list_part = []
    banned_list = d_author_type_1.iloc[i, :]['reviewer_id']
    banned_list = banned_list + global_banned_list

    # 2个0号单位评委；

    candidate_list_all = diff(reviewer_list_type_0, banned_list)
    random.shuffle(candidate_list_all)
    candidate_list_new = find_best_from_candiate(
        d_author_type_1.iloc[i, :]
        ['文章领域（1资产定价；2公司金融；3金融中介/银行/金融机构；4宏观金融；5其他）'], candidate_list_all,
        d_reviewer)
    if len(candidate_list_new) >= 2:
        reviewer_list_part = reviewer_list_part + (random.sample(
            candidate_list_new, 2))
    else:
        reviewer_list_part = reviewer_list_part + candidate_list_new
        reviewer_list_part = reviewer_list_part + \
            (random.sample(diff(candidate_list_all, candidate_list_new), 2-len(candidate_list_new)))

    # 1个1号单位评委；
    candidate_list_all = diff(reviewer_list_type_1, banned_list)

    candidate_list_new = find_best_from_candiate(
        d_author_type_1.iloc[i, :]
        ['文章领域（1资产定价；2公司金融；3金融中介/银行/金融机构；4宏观金融；5其他）'], candidate_list_all,
        d_reviewer)
    if len(candidate_list_new) >= 1:
        reviewer_list_part = reviewer_list_part + (random.sample(
            candidate_list_new, 1))
    else:
        reviewer_list_part = reviewer_list_part + candidate_list_new
        reviewer_list_part = reviewer_list_part + \
            (random.sample(diff(candidate_list_all, candidate_list_new), 1-len(candidate_list_new)))

    # 1个2号单位评委；
    candidate_list_all = diff(reviewer_list_type_2, banned_list)

    candidate_list_new = find_best_from_candiate(
        d_author_type_1.iloc[i, :]
        ['文章领域（1资产定价；2公司金融；3金融中介/银行/金融机构；4宏观金融；5其他）'], candidate_list_all,
        d_reviewer)
    if len(candidate_list_new) >= 1:
        reviewer_list_part = reviewer_list_part + (random.sample(
            candidate_list_new, 1))
    else:
        reviewer_list_part = reviewer_list_part + candidate_list_new
        reviewer_list_part = reviewer_list_part + \
            (random.sample(diff(candidate_list_all, candidate_list_new), 1-len(candidate_list_new)))

    # 1个3号单位评委。
    candidate_list_all = diff(reviewer_list_type_3, banned_list)

    candidate_list_new = find_best_from_candiate(
        d_author_type_1.iloc[i, :]
        ['文章领域（1资产定价；2公司金融；3金融中介/银行/金融机构；4宏观金融；5其他）'], candidate_list_all,
        d_reviewer)
    if len(candidate_list_new) >= 1:
        reviewer_list_part = reviewer_list_part + (random.sample(
            candidate_list_new, 1))
    else:
        reviewer_list_part = reviewer_list_part + candidate_list_new
        reviewer_list_part = reviewer_list_part + \
            (random.sample(diff(candidate_list_all, candidate_list_new), 1-len(candidate_list_new)))

    global_banned_list = global_banned_list + check_banner(reviewer_list_part)
    reviewer_list_all_list.append(reviewer_list_part)
d_author_type_1['reviewer_assing'] = reviewer_list_all_list

d_author_new = pd.concat([
    d_author_type_1[['序号', 'reviewer_assing']],
    d_author_type_2[['序号', 'reviewer_assing']],
    d_author_type_3[['序号', 'reviewer_assing']]
])

d_author_new['reviewer1'] = d_author_new.apply(lambda row: row[
    'reviewer_assing'][0],
                                               axis=1)
d_author_new['reviewer2'] = d_author_new.apply(lambda row: row[
    'reviewer_assing'][1],
                                               axis=1)
d_author_new['reviewer3'] = d_author_new.apply(lambda row: row[
    'reviewer_assing'][2],
                                               axis=1)
d_author_new['reviewer4'] = d_author_new.apply(lambda row: row[
    'reviewer_assing'][3],
                                               axis=1)
d_author_new['reviewer5'] = d_author_new.apply(lambda row: row[
    'reviewer_assing'][4],
                                               axis=1)

d_author_origin = pd.merge(d_author_origin, d_author_new, how='left', on='序号')
d_author_origin.drop(['reviewer_assing', 'org', 'reviewer_id'],
                     axis=1,
                     inplace=True)

d_author_origin.to_excel('作者_new.xlsx', index=False)

excel_path = '作者_new.xlsx'
d_author = pd.read_excel(excel_path)

d_reviewer_new = d_reviewer_origin

reviewer_paper = []
reviewer_id_list = d_reviewer_new['序号'].values.tolist()

for reviewer_id in reviewer_id_list:
    revieweer_temp_list = []

    revieweer_temp_list = revieweer_temp_list + (
        d_author.loc[d_author['reviewer1'] ==
                     reviewer_id, '序号'].values.tolist())

    revieweer_temp_list = revieweer_temp_list + (
        d_author.loc[d_author['reviewer2'] ==
                     reviewer_id, '序号'].values.tolist())

    revieweer_temp_list = revieweer_temp_list + (
        d_author.loc[d_author['reviewer3'] ==
                     reviewer_id, '序号'].values.tolist())

    revieweer_temp_list = revieweer_temp_list + (
        d_author.loc[d_author['reviewer4'] ==
                     reviewer_id, '序号'].values.tolist())

    revieweer_temp_list = revieweer_temp_list + (
        d_author.loc[d_author['reviewer5'] ==
                     reviewer_id, '序号'].values.tolist())
    reviewer_paper.append(revieweer_temp_list)

d_reviewer_new['paper_id'] = reviewer_paper

d_reviewer_new['paper_numbers'] = d_reviewer_new.apply(lambda row: len(row[
    'paper_id']),
                                                       axis=1)
print(max(d_reviewer_new['paper_numbers'].values.tolist()))
print(min(d_reviewer_new['paper_numbers'].values.tolist()))
number_list = d_reviewer_new['paper_numbers'].values.tolist()
number_list.sort()
print(number_list)
d_reviewer_new.to_excel('评委_new.xlsx', index=False)
