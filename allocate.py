from copy import copy
import pandas as pd
import bisect
import random
from openpyxl import load_workbook
from openpyxl.utils import get_column_letter
import os
import shutil
import os
import time


def pandas_to_excel(df, fnm):
    writer = pd.ExcelWriter(
        fnm,
        engine='xlsxwriter',
    )

    # Convert the dataframe to an XlsxWriter Excel object. Note that we turn off
    # the default header and skip one row to allow us to insert a user defined
    # header.

    df.to_excel(writer,
                sheet_name='Sheet1',
                startrow=1,
                header=False,
                index=False)

    # Get the xlsxwriter workbook and worksheet objects.
    workbook = writer.book
    worksheet = writer.sheets['Sheet1']

    # Add a header format.
    header_format = workbook.add_format({
        'bold': True,
        'text_wrap': True,
        'valign': 'top',
        'border': 1,
        'font_name': 'SimHei',
        'font_color': '#FFFFFF',
        'fg_color': '#436089',
    })

    # Write the column headers with the defined format.
    for col_num, value in enumerate(df.columns.values):
        worksheet.write(0, col_num, value, header_format)
    format1 = workbook.add_format({'font_name': 'SimHei'})
    format2 = workbook.add_format({
        'font_name': 'SimHei',
        'font_color': '#B2281D'
    })
    worksheet.set_column('A:A', 8, format2)
    worksheet.set_column('B:B', 100, format1)
    worksheet.set_column('C:C', 20, format1)
    worksheet.set_column('D:D', 80, format1)
    writer.save()


'''
 该函数主要的参数为io、sheetname、header、names、encoding。
 io:excel文件，可以是文件路径、文件网址、file-like对象、xlrd workbook;
 sheetname:返回指定的sheet，参数可以是字符串（sheet名）、整型（sheet索引）、list（元素为字符串和整型，返回字典{'key':'sheet'}）、none（返回字典，全部sheet）;
 header:指定数据表的表头，参数可以是int、list of ints，即为索引行数为表头;
 names:返回指定name的列，参数为array-like对象。
 encoding:关键字参数，指定以何种编码读取。
 该函数返回pandas中的DataFrame或dict of DataFrame对象，利用DataFrame的相关操作即可读取相应的数据。
 '''
# 代码示例:
excel_path = '评委与论文.xlsx'
d_reviewer = pd.read_excel(excel_path)

excel_path = 'Copy of 论文与评委.xlsx'
d_author = pd.read_excel(excel_path)


def get_name_by_id(paper_id):

    return str(d_author.loc[d_author['序号'] == int(paper_id)]['标题'].values[0])


def copy_file_to_path(paper_id, path):
    dr = '/Users/kitty_4016/workspace/review_laura/233篇'
    match = [f for f in os.listdir(dr) if f.startswith(paper_id + ".")]

    if match:
        match = match[0]
        shutil.copy(
            os.path.join(dr, match),
            os.path.join('/Users/kitty_4016/workspace/review_laura', path))
        new_src_file_name = os.path.join(
            os.path.join('/Users/kitty_4016/workspace/review_laura', path),
            match)

        new_dst_file_name = os.path.join(
            os.path.join('/Users/kitty_4016/workspace/review_laura',
                         path), paper_id + '-' +
            get_name_by_id(int(paper_id)) + '.' + match.split('.')[1])

        os.rename(new_src_file_name, new_dst_file_name)  # rename
        # etc...

    else:
        print("No such file exists.  Please try again.")


def generate_reviewer_excel(row):
    paper_num = row['paper_numbers']
    if (paper_num != 0):
        # if (row['paper_numbers'] != 0):

        path = "output/{}".format(row['姓名'])

        #
        list_temp_all = []
        os.mkdir(path)
        for paper_id in row['paper_id'].strip('][').split(', '):

            list_temp = []
            list_temp.append(paper_id)
            list_temp.append(get_name_by_id(paper_id))
            list_temp.append('')
            list_temp.append('')
            copy_file_to_path(paper_id, path)

            list_temp_all.append(list_temp)

        df_new = pd.DataFrame(
            list_temp_all,
            columns=['文章序号', '论文题目', '论文评分（最高5分，最低1分）', '其他意见 (可以不填）'])
        try:

            # df_new.style.set_table_styles([{'selector': 'th', 'props': [('background-color', 'red')]}])\
            #     .to_excel(,
            #               index=False, engine='openpyxl')
            pandas_to_excel(df_new,
                            "{}/{}-论文评审意见表.xlsx".format(path, row['姓名']))

        except OSError:
            print("Creation of the directory %s failed" % path)
        else:
            print("Successfully created the directory %s " % path)


d_reviewer.apply(lambda row: generate_reviewer_excel(row), axis=1)
print(d_reviewer.head())
wb = load_workbook('论文评审意见表.xlsx')

default_sheet = wb['Sheet1']
ws = wb.active
default_col_width = ws.sheet_format.defaultColWidth
new_sheet = wb.create_sheet('Sheet2')

row1 = next(default_sheet.rows)

row1 = next(default_sheet.rows)
for cell in row1:
    new_cell = new_sheet.cell(row=cell.row,
                              column=cell.col_idx,
                              value=cell.value)
    print(ws.column_dimensions[get_column_letter(cell.col_idx + 1)].width)
    if cell.has_style:
        new_cell.font = copy(cell.font)
        new_cell.border = copy(cell.border)
        new_cell.fill = copy(cell.fill)
        new_cell.number_format = copy(cell.number_format)
        new_cell.protection = copy(cell.protection)
        new_cell.alignment = copy(cell.alignment)

wb.save(filename='sample_book.xlsx')
